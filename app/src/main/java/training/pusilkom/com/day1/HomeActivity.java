package training.pusilkom.com.day1;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final TextView textView = (TextView) findViewById(R.id.textView2);

        textView.setText(getIntent().getStringExtra("name"));

        Context context = getApplicationContext();

        String message = getIntent().getStringExtra("name");

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(R.string.dialog_title)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .create();

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.setTitle(R.string.dialog_title);
        dialog.setContentView(R.layout.dialog_info);
//        dialog.show();

        final EditText input = (EditText) dialog.findViewById(R.id.dialogInput);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(input.getText());
                dialog.dismiss();
            }
        });

        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();

    }
}
